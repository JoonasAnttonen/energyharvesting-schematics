EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:EnergyHarvesting
LIBS:EnergyHarvesting-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L bq25505 U4
U 1 1 5887440D
P 4350 3000
F 0 "U4" H 3900 3450 60  0000 C CNN
F 1 "bq25505" H 4350 3000 60  0000 C CNN
F 2 "Housings_DFN_QFN:QFN-20-1EP_4x4mm_Pitch0.5mm" H 4350 3000 39  0001 C CNN
F 3 "" H 4350 3000 60  0000 C CNN
	1    4350 3000
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C1
U 1 1 58874561
P 2900 2500
F 0 "C1" H 2950 2600 39  0000 L CNN
F 1 "4.7u" H 2950 2400 39  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 2938 2350 30  0001 C CNN
F 3 "" H 2900 2500 60  0000 C CNN
	1    2900 2500
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C2
U 1 1 58874596
P 3200 2500
F 0 "C2" H 3250 2600 39  0000 L CNN
F 1 "100n" H 3250 2400 39  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3238 2350 30  0001 C CNN
F 3 "" H 3200 2500 60  0000 C CNN
	1    3200 2500
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C6
U 1 1 588745DA
P 5100 2350
F 0 "C6" H 5150 2450 39  0000 L CNN
F 1 "47m" H 5150 2250 39  0000 L CNN
F 2 "EnergyHarvesting:Supercapacitor" H 5138 2200 30  0001 C CNN
F 3 "" H 5100 2350 60  0000 C CNN
	1    5100 2350
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C4
U 1 1 58874603
P 4400 1750
F 0 "C4" H 4450 1850 39  0000 L CNN
F 1 "4.7u" H 4450 1650 39  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 4438 1600 30  0001 C CNN
F 3 "" H 4400 1750 60  0000 C CNN
	1    4400 1750
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C5
U 1 1 5887463E
P 4700 1750
F 0 "C5" H 4750 1850 39  0000 L CNN
F 1 "100n" H 4750 1650 39  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 4738 1600 30  0001 C CNN
F 3 "" H 4700 1750 60  0000 C CNN
	1    4700 1750
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C3
U 1 1 58874673
P 3250 3300
F 0 "C3" H 3300 3400 39  0000 L CNN
F 1 "10n" H 3300 3200 39  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3288 3150 30  0001 C CNN
F 3 "" H 3250 3300 60  0000 C CNN
	1    3250 3300
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR L1
U 1 1 58874709
P 3800 2300
F 0 "L1" V 3950 2300 50  0000 C CNN
F 1 "22u" V 3750 2300 39  0000 C CNN
F 2 "EnergyHarvesting:Coilcraft_LPS4018" H 3800 2300 60  0001 C CNN
F 3 "" H 3800 2300 60  0000 C CNN
	1    3800 2300
	0    -1   -1   0   
$EndComp
$Comp
L Resistor RIN1
U 1 1 588748BB
P 2700 2300
F 0 "RIN1" H 2600 2400 39  0000 L CNN
F 1 "100" H 2600 2200 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 2700 2230 30  0001 C CNN
F 3 "" V 2700 2300 30  0000 C CNN
	1    2700 2300
	1    0    0    -1  
$EndComp
$Comp
L Resistor ROV1
U 1 1 58874AD8
P 4550 4000
F 0 "ROV1" H 4500 3800 39  0000 L CNN
F 1 "5M" H 4550 3900 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 4550 3930 30  0001 C CNN
F 3 "" V 4550 4000 30  0000 C CNN
	1    4550 4000
	-1   0    0    1   
$EndComp
$Comp
L Resistor ROV2
U 1 1 58874C32
P 4350 4200
F 0 "ROV2" V 4300 4300 39  0000 L CNN
F 1 "4M" V 4400 4300 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 4350 4130 30  0001 C CNN
F 3 "" V 4350 4200 30  0000 C CNN
	1    4350 4200
	0    1    1    0   
$EndComp
$Comp
L Resistor ROK1
U 1 1 58874C85
P 5600 2750
F 0 "ROK1" V 5550 2850 39  0000 L CNN
F 1 "3M" V 5650 2850 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 5600 2680 30  0001 C CNN
F 3 "" V 5600 2750 30  0000 C CNN
	1    5600 2750
	0    1    1    0   
$EndComp
$Comp
L Resistor ROK2
U 1 1 58874CBC
P 5600 3150
F 0 "ROK2" V 5550 3250 39  0000 L CNN
F 1 "3M" V 5650 3250 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 5600 3080 30  0001 C CNN
F 3 "" V 5600 3150 30  0000 C CNN
	1    5600 3150
	0    1    1    0   
$EndComp
$Comp
L Resistor ROK3
U 1 1 58874D05
P 5600 3550
F 0 "ROK3" V 5550 3650 39  0000 L CNN
F 1 "3M" V 5650 3650 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 5600 3480 30  0001 C CNN
F 3 "" V 5600 3550 30  0000 C CNN
	1    5600 3550
	0    1    1    0   
$EndComp
$Comp
L Resistor ROUT1
U 1 1 58874E1C
P 5350 1550
F 0 "ROUT1" H 5250 1650 39  0000 L CNN
F 1 "100" H 5250 1450 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 5350 1480 30  0001 C CNN
F 3 "" V 5350 1550 30  0000 C CNN
	1    5350 1550
	1    0    0    -1  
$EndComp
$Comp
L GNDREF #PWR01
U 1 1 588753F9
P 2100 4300
F 0 "#PWR01" H 2100 4050 50  0001 C CNN
F 1 "GNDREF" H 2100 4150 50  0001 C CNN
F 2 "" H 2100 4300 60  0000 C CNN
F 3 "" H 2100 4300 60  0000 C CNN
	1    2100 4300
	1    0    0    -1  
$EndComp
$Comp
L GNDREF #PWR02
U 1 1 58878EEB
P 4350 4400
F 0 "#PWR02" H 4350 4150 50  0001 C CNN
F 1 "GNDREF" H 4350 4250 50  0001 C CNN
F 2 "" H 4350 4400 60  0000 C CNN
F 3 "" H 4350 4400 60  0000 C CNN
	1    4350 4400
	1    0    0    -1  
$EndComp
NoConn ~ 4950 2900
$Comp
L GNDREF #PWR03
U 1 1 5887EECB
P 3050 2700
F 0 "#PWR03" H 3050 2450 50  0001 C CNN
F 1 "GNDREF" H 3050 2550 50  0001 C CNN
F 2 "" H 3050 2700 60  0000 C CNN
F 3 "" H 3050 2700 60  0000 C CNN
	1    3050 2700
	1    0    0    -1  
$EndComp
$Comp
L GNDREF #PWR04
U 1 1 588780E2
P 3250 3500
F 0 "#PWR04" H 3250 3250 50  0001 C CNN
F 1 "GNDREF" H 3250 3350 50  0001 C CNN
F 2 "" H 3250 3500 60  0000 C CNN
F 3 "" H 3250 3500 60  0000 C CNN
	1    3250 3500
	1    0    0    -1  
$EndComp
NoConn ~ 4550 3600
NoConn ~ 4450 3600
Text GLabel 5100 1450 1    39   Input ~ 0
VOUT_HIGH
Text GLabel 3750 3000 0    39   Input ~ 0
VSTOR
Text GLabel 2500 2200 1    39   Input ~ 0
VIN_HIGH
Text GLabel 2900 2200 1    39   Input ~ 0
VIN_LOW
$Comp
L GNDREF #PWR05
U 1 1 5888A20F
P 5100 2550
F 0 "#PWR05" H 5100 2300 50  0001 C CNN
F 1 "GNDREF" H 5100 2400 50  0001 C CNN
F 2 "" H 5100 2550 60  0000 C CNN
F 3 "" H 5100 2550 60  0000 C CNN
	1    5100 2550
	1    0    0    -1  
$EndComp
Text GLabel 2150 4300 2    39   Input ~ 0
GND
Text GLabel 4950 2800 2    39   Input ~ 0
GND
Text GLabel 3750 2800 0    39   Input ~ 0
GND
Text GLabel 4950 3000 2    39   Input ~ 0
DP6
$Comp
L GNDREF #PWR06
U 1 1 588914A0
P 4550 1950
F 0 "#PWR06" H 4550 1700 50  0001 C CNN
F 1 "GNDREF" H 4550 1800 50  0001 C CNN
F 2 "" H 4550 1950 60  0000 C CNN
F 3 "" H 4550 1950 60  0000 C CNN
	1    4550 1950
	1    0    0    -1  
$EndComp
Text GLabel 3750 3200 0    39   Input ~ 0
GND
Text GLabel 4150 3600 3    39   Input ~ 0
GND
Text GLabel 4550 2400 1    39   Input ~ 0
GND
Text GLabel 4450 2400 1    39   Input ~ 0
GND
Text GLabel 4950 3300 2    39   Input ~ 0
GND
Text GLabel 4750 4000 2    39   Input ~ 0
VRDIV
Text GLabel 5600 2550 1    39   Input ~ 0
VRDIV
$Comp
L GNDREF #PWR07
U 1 1 588A31C2
P 5600 3750
F 0 "#PWR07" H 5600 3500 50  0001 C CNN
F 1 "GNDREF" H 5600 3600 50  0001 C CNN
F 2 "" H 5600 3750 60  0000 C CNN
F 3 "" H 5600 3750 60  0000 C CNN
	1    5600 3750
	1    0    0    -1  
$EndComp
Text GLabel 4350 3600 3    39   Input ~ 0
VRDIV
Text Notes 4700 4250 0    39   ~ 0
Storage Overvoltage Protection\n3/2*VBIAS*(1+(ROV2/ROV1)) = approx. 4V
Text Notes 5900 3200 0    39   ~ 0
Storage Voltage Operating Range\nDecreasing (OK_PROG) = VBIAS*(1+(ROK2/ROK1)) = 2.4V (VBAT_OK goes LOW)\nIncreasing (OK_HYST)= VBIAS*(1+((ROK2+ROK3)/ROK1)) = 3.6V (VBAT_OK goes HIGH)
Text Notes 2450 1850 0    39   ~ 0
Input Current Measurement\nI=(VIN_HIGH-VIN_LOW)/RIN\nI<200uA\nV across RIN not significant
Text Notes 5050 1050 0    39   ~ 0
Output Current Measurement\nI=(VOUT_HIGH-VOUT_LOW)/ROUT\n10uA<I<10mA\nV across ROUT might be significant when storage V is low
Text GLabel 5700 1450 1    39   Input ~ 0
V_out
Text Notes 5100 2100 0    39   ~ 0
Supercapacitor Energy Storage
Text Notes 2950 2250 0    39   ~ 0
Input Decoupling
Text Notes 4450 1500 0    39   ~ 0
Output Decoupling
$Comp
L SolarBIT U1
U 1 1 58877DE8
P 2100 2650
F 0 "U1" V 2100 2850 60  0000 C CNN
F 1 "SolarBIT" H 2100 2450 60  0001 C CNN
F 2 "EnergyHarvesting:SolarBIT" H 2100 2650 60  0001 C CNN
F 3 "" H 2100 2650 60  0000 C CNN
	1    2100 2650
	0    -1   -1   0   
$EndComp
$Comp
L SolarBIT U2
U 1 1 588791F8
P 2100 3300
F 0 "U2" V 2100 3500 60  0000 C CNN
F 1 "SolarBIT" H 2100 3100 60  0001 C CNN
F 2 "EnergyHarvesting:SolarBIT" H 2100 3300 60  0001 C CNN
F 3 "" H 2100 3300 60  0000 C CNN
	1    2100 3300
	0    -1   -1   0   
$EndComp
$Comp
L SolarBIT U3
U 1 1 5887932E
P 2100 3950
F 0 "U3" V 2100 4150 60  0000 C CNN
F 1 "SolarBIT" H 2100 3750 60  0001 C CNN
F 2 "EnergyHarvesting:SolarBIT" H 2100 3950 60  0001 C CNN
F 3 "" H 2100 3950 60  0000 C CNN
	1    2100 3950
	0    -1   -1   0   
$EndComp
Text Notes 800  3250 0    39   ~ 0
Solar Input (KXOB22-12X1L)\nOpen Circuit = 3*600mV = 1.9V\nMPP Voltage = 3*500mV = 1.5V
$Comp
L Schottky_Diode D1
U 1 1 5887C256
P 2300 2300
F 0 "D1" V 2150 2250 39  0000 L CNN
F 1 "Schottky_Diode" H 2350 2200 39  0001 L CNN
F 2 "Diodes_SMD:D_0805" H 2300 2300 60  0001 C CNN
F 3 "" H 2300 2300 60  0000 C CNN
	1    2300 2300
	0    1    1    0   
$EndComp
Text Notes 2900 3700 0    39   ~ 0
Regulator Voltage Storage
$Comp
L HERMAPHRODITIC J1
U 1 1 58883B4C
P 2850 5850
F 0 "J1" H 2850 6150 60  0000 C CNN
F 1 "HERMAPHRODITIC" H 3050 6300 60  0001 C CNN
F 2 "EnergyHarvesting:Hermophroditic" H 2850 5850 60  0001 C CNN
F 3 "" H 2850 5850 60  0000 C CNN
	1    2850 5850
	1    0    0    -1  
$EndComp
$Comp
L HERMAPHRODITIC J2
U 1 1 58883C3B
P 4500 5850
F 0 "J2" H 4500 6150 60  0000 C CNN
F 1 "HERMAPHRODITIC" H 4700 6300 60  0001 C CNN
F 2 "EnergyHarvesting:Hermophroditic" H 4500 5850 60  0001 C CNN
F 3 "" H 4500 5850 60  0000 C CNN
	1    4500 5850
	1    0    0    -1  
$EndComp
Text GLabel 3100 5650 2    39   Input ~ 0
VDD
Text GLabel 2600 5650 0    39   Input ~ 0
GND
Text GLabel 3100 5850 2    39   Input ~ 0
DP12/TDO
Text GLabel 3100 5950 2    39   Input ~ 0
DP11/CSN
Text GLabel 3100 6150 2    39   Input ~ 0
DP10/MOSI
Text GLabel 3100 6250 2    39   Input ~ 0
DP9/MISO
Text GLabel 3100 6350 2    39   Input ~ 0
DP8/SCLK/TDI
Text GLabel 3100 5750 2    39   Input ~ 0
SCL
Text GLabel 2600 5750 0    39   Input ~ 0
SDA
Text GLabel 2600 6250 0    39   Input ~ 0
VOUT_LOW
Text GLabel 2600 6350 0    39   Input ~ 0
VOUT_HIGH
Text GLabel 2600 6450 0    39   Input ~ 0
VIN_LOW
Text GLabel 2600 6550 0    39   Input ~ 0
VIN_HIGH
Text Notes 2700 5400 0    39   ~ 0
SensorTag Connector (In)
Text Notes 4400 5400 0    39   ~ 0
LCD Connector (Out)
Wire Wire Line
	2100 2300 2150 2300
Wire Wire Line
	2850 2300 3500 2300
Wire Wire Line
	4100 2300 4150 2300
Wire Wire Line
	4150 2300 4150 2400
Wire Wire Line
	3750 3200 3750 3200
Wire Wire Line
	3250 3450 3250 3500
Wire Wire Line
	3250 3100 3750 3100
Wire Wire Line
	3250 3100 3250 3150
Wire Wire Line
	4350 4000 4350 4050
Wire Wire Line
	4250 4000 4400 4000
Wire Wire Line
	4250 3600 4250 4000
Connection ~ 4350 4000
Wire Wire Line
	5600 2900 5600 3000
Wire Wire Line
	5600 3300 5600 3400
Wire Wire Line
	4950 3200 5300 3200
Connection ~ 5600 2950
Wire Wire Line
	5300 3100 4950 3100
Connection ~ 5600 3350
Wire Wire Line
	3200 2300 3200 2350
Connection ~ 3200 2300
Wire Wire Line
	2900 2200 2900 2350
Connection ~ 2900 2300
Wire Wire Line
	3200 2700 3200 2650
Wire Wire Line
	2900 2700 3200 2700
Connection ~ 3050 2700
Wire Wire Line
	2900 2700 2900 2650
Wire Wire Line
	4250 1450 4250 2400
Wire Wire Line
	3750 3000 3750 3000
Wire Wire Line
	2450 2300 2550 2300
Wire Wire Line
	2500 2300 2500 2200
Connection ~ 2500 2300
Wire Wire Line
	4350 2400 4350 2150
Wire Wire Line
	5100 2500 5100 2550
Wire Wire Line
	4950 2800 4950 2800
Wire Wire Line
	3750 2800 3750 2800
Wire Wire Line
	4950 3000 4950 3000
Wire Wire Line
	4700 1950 4700 1900
Wire Wire Line
	4400 1950 4700 1950
Wire Wire Line
	4400 1950 4400 1900
Connection ~ 4550 1950
Wire Wire Line
	4700 1550 4700 1600
Wire Wire Line
	4250 1550 5200 1550
Wire Wire Line
	4400 1550 4400 1600
Connection ~ 4400 1550
Connection ~ 4700 1550
Wire Wire Line
	5600 2950 5300 2950
Wire Wire Line
	5300 2950 5300 3100
Wire Wire Line
	5300 3350 5600 3350
Wire Wire Line
	5300 3200 5300 3350
Wire Wire Line
	4350 4350 4350 4400
Wire Wire Line
	5600 3750 5600 3700
Wire Wire Line
	5600 2550 5600 2600
Wire Wire Line
	4750 4000 4700 4000
Wire Wire Line
	2100 4250 2100 4300
Wire Wire Line
	2100 2300 2100 2350
Wire Wire Line
	2100 2950 2100 3000
Wire Wire Line
	2100 3600 2100 3650
Wire Wire Line
	5100 1450 5100 1550
Connection ~ 5100 1550
Wire Wire Line
	5500 1550 5700 1550
Wire Wire Line
	2100 4300 2150 4300
Connection ~ 2100 4300
Wire Wire Line
	4350 2150 5100 2150
Wire Wire Line
	5100 2150 5100 2200
Wire Wire Line
	3450 2300 3450 2900
Connection ~ 3450 2300
Wire Wire Line
	3450 2900 3750 2900
Text GLabel 4250 5650 0    39   Input ~ 0
VDD
Text GLabel 4750 5650 2    39   Input ~ 0
GND
Text GLabel 4250 5750 0    39   Input ~ 0
SCL
Text GLabel 4750 5750 2    39   Input ~ 0
SDA
Text GLabel 4250 5850 0    39   Input ~ 0
DP12/TDO
Text GLabel 4250 5950 0    39   Input ~ 0
DP11/CSN
Text GLabel 4250 6350 0    39   Input ~ 0
DP8/SCLK/TDI
Text GLabel 4250 6250 0    39   Input ~ 0
DP9/MISO
Text GLabel 4250 6150 0    39   Input ~ 0
DP10/MOSI
Text Notes 6300 3850 0    39   ~ 0
VBIAS = approx. 1.2V
Text Notes 2500 5200 0    39   ~ 0
Incoming and Outgoing connections are mirror images because of connector topology
Wire Wire Line
	5550 1550 5550 1450
Wire Wire Line
	5700 1550 5700 1450
Connection ~ 5550 1550
Text GLabel 5550 1450 1    39   Input ~ 0
VOUT_LOW
Text GLabel 4250 1450 1    39   Input ~ 0
VSTOR
Connection ~ 4250 1550
NoConn ~ 4250 6550
NoConn ~ 3100 6550
NoConn ~ 4750 6250
NoConn ~ 4750 6350
NoConn ~ 4750 6450
NoConn ~ 4750 6550
NoConn ~ 4750 5850
NoConn ~ 4750 5950
NoConn ~ 4750 6050
NoConn ~ 4750 6150
NoConn ~ 2600 5850
NoConn ~ 2600 5950
NoConn ~ 2600 6050
NoConn ~ 2600 6150
NoConn ~ 3100 6450
NoConn ~ 4250 6450
NoConn ~ 4250 6050
NoConn ~ 3100 6050
Text GLabel 2600 5950 0    39   Input ~ 0
DP6
$EndSCHEMATC
